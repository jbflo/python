__author__ = 'jb-flo'


"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 9

"""

# Exercise XII.1. The “rank” of a word is its position in a list of words sorted by frequency:
# the most common word has rank 1, the second most common has rank 2, etc.
# Zipf’s law describes a relationship between the ranks and frequencies of words in natural languages
# (http://en.wikipedia.org/wiki/Zipf's_law). Speciﬁcally, it predicts that the frequency, f, of the word with rank r is:
# f = cr−s  where s and c are parameters that depend on the language and the text.
# If you take the logarithm of both sides of this equation, you get:
# log f = log c – s x log r
# So if you plot log f versus log r, you should get a straight line with slope−s and intercept log c.
# Write a program that reads a text from a ﬁle, counts word frequencies,
#  and prints one line for each word, in descending order of frequency, with log f and log r.
# Use the graphing program of your choice to plot the results and check whether they form a straight line.
# Can you estimate the value of s?
# To make the plots, you might have to install matplotlib (see http://matplotlib.sourceforge.net/).
# (30 points)


import sys
import string
import random
import matplotlib.pyplot as pyplot


def process_file(filename, skip_header):
    """ first fuction that contain  histogram that contains the words from a file.

    filename: string
    skip_header: boolean, whether to skip the Gutenberg header

    Returns: map from each word to the number of times it appears.
    """
    hist = {}
    fp = open(filename)

    if skip_header:
        skip_gutenberg_header(fp)

    for line in fp:
        process_line(line, hist)
    return hist


def skip_gutenberg_header(fp):
    """Reads from fp until it finds the line that ends the header.

    fp: open file object
    """
    for line in fp:
        if line.startswith('*** START OF THIS PROJECT GUTENBERG EBOOK LE MULTILINGUISME'):
            break


def process_line(line, hist):
    """Adds the words in the line to the histogram.

    Modifies hist.

    line: string
    hist: histogram (map from word to frequency)
    """
    # replace hyphens with spaces before splitting
    line = line.replace('-', ' ')

    for word in line.split():
        # remove punctuation and convert to lowercase
        word = word.strip(string.punctuation + string.whitespace)
        word = word.lower()

        # update the histogram
        hist[word] = hist.get(word, 0) + 1


def most_common(hist):
    """Makes a list of the key-value pairs from a histogram and
    sorts them in descending order by frequency."""
    t = []
    for key, value in hist.items():
        t.append((value, key))

    t.sort()
    t.reverse()
    return t


def print_most_common(hist, num=10):
    """Prints the most commons words in a histgram and their frequencies.

    hist: histogram (map from word to frequency
    num: number of words to print
    """
    t = most_common(hist)
    print ('The most common words are:' )
    for freq, word in t[:num]:
        print (word, '\t', freq )


def subtract(d1, d2):
    """Returns a dictionary with all keys that appear in d1 but not d2.

    d1, d2: dictionaries
    """
    res = {}
    for key in d1:
        if key not in d2:
            res[key] = None
    return res


def total_words(hist):
    """Returns the total of the frequencies in a histogram."""
    return sum(hist.values())


def different_words(hist):
    """Returns the number of different words in a histogram."""
    return len(hist)


def random_word(hist):
    """Chooses a random word from a histogram.

    The probability of each word is proportional to its frequency.
    """
    t = []
    for word, freq in hist.items():
        t.extend([word] * freq)

    return random.choice(t)


def rank_freq(hist):
    """Returns a list of tuples where each tuple is a rank
    and the number of times the item with that rank appeared.
    """
    # sort the list of frequencies in decreasing order
    freqs = hist.values()
    list(freqs).sort(reverse=True)

    # enumerate the ranks and frequencies
    rf = [(r+1, f) for r, f in enumerate(freqs)]
    return rf


def print_ranks(hist):
    """Prints the rank vs. frequency data."""
    for r, f in rank_freq(hist):
        print( r, f )


def plot_ranks(hist, scale='log'):
    """Plots frequency vs. rank."""
    t = rank_freq(hist)
    rs, fs = zip(*t)

    pyplot.clf()
    pyplot.xscale(scale)
    pyplot.yscale(scale)
    pyplot.title('Florial Jean Baptiste Solution 9 plot')
    pyplot.xlabel('rank of Dorian')
    pyplot.ylabel('frequency')
    pyplot.plot(rs, fs, 'r-')
    pyplot.show()


def main(name, filename='multilinguisme.txt', flag='plot', *args):
    hist = process_file(filename, skip_header=True)

    # either print the results or plot them
    if flag == 'print':
        print_ranks(hist)
    elif flag == 'plot':
        plot_ranks(hist)
    else:
        print('Usage: Solution 9 filename [print|plot]')


if __name__ == '__main__':
    main(*sys.argv)

