__author__ = 'jb-flo'


"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 8

"""

# Exercise VIII.1. Markov analysis:
# 1. Write a program to read a text from a ﬁle and perform Markov analysis.
# The result should be a dictionary that maps from preﬁxes to a collection of possible sufﬁxes.
#  The collection might be a list, tuple, or dictionary; it is up to you to make an appropriate choice.
# You can test your program with preﬁx length two,
# but you should write the program in a way that makes it easy to try other lengths.
# 2. Add a function to the previous program to generate random text based on the Markov analysis.
# Here is an example from Emma with preﬁx length 2:
# He was very clever, be it sweetness or be angry, ashamed or only amused, at such a stroke.
# She had never thought of Hannah till you were never meant for me?" "I cannot make speeches,
# Emma:" he soon cut it all himself.
# For this example, I left the punctuation attached to the words.
# The result is almost syntactically correct, but not quite. Semantically, it almost makes sense, but not quite.
# What happens if you increase the preﬁx length? Does the random text make more sense?
# 3. Once your program is working, you might want to try a mash-up: if you analyze text from two or more books,
#  the random text you generate will blend the vocabulary and phrases from the sources in interesting ways.
# Credit: This case study is based on an example from Kernighan and Pike, The Practice of Programming,
#  Addison-Wesley, 1999.



import sys
import string
import random

# global variables
suffix_map = {}        # map from prefixes to a list of suffixes
prefix = ()            # current tuple of words


def process_file(filename, order=2):

    fp = open(filename)
    skip_gutenberg_header(fp)

    for line in fp:
        for word in line.rstrip().split():
            process_word(word, order)


def skip_gutenberg_header(fp):

    for line in fp:
        if line.startswith('*** START OF THIS PROJECT GUTENBERG '):
            break


def process_word(word, order=2):

    global prefix
    if len(prefix) < order:
        prefix += (word,)
        return

    try:
        suffix_map[prefix].append(word)
    except KeyError:
        # if there is no entry for this prefix, make one
        suffix_map[prefix] = [word]

    prefix = shift(prefix, word)


def random_text(n=100):

    # choose a random prefix (not weighted by frequency)
    start = random.choice(suffix_map.keys())

    for i in range(n):
        suffixes = suffix_map.get(start, None)
        if suffixes == None:
            # if the start isn't in map, we got to the end of the
            # original text, so we have to start again.
            random_text(n-i)
            return

        # choose a random suffix
        word = random.choice(suffixes)
        print (word,)
        start = shift(start, word)


def shift(t, word):

    return t[1:] + (word,)


def main(name, filename='', n=100, order=2, *args):
    try:
        n = int(n)
        order = int(order)
    except:
        print ('Usage: randomtext.py filename [# of words] [prefix length]')
    else:
        process_file("multilinguisme.txt", order)
        random_text(n)


if __name__ == '__main__':
    main(*sys.argv)
