__author__ = 'jb-flo'

"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 2

"""

# Exercise I.2. Go to Project Gutenberg (http://gutenberg.org)
# and download your favorite out-of-copyright book in plain text format.
# Modify your program from the previous exercise to read the book you downloaded,
# skip over the header information at the beginning of the ﬁle, and process the rest of the words as before.
# Then modify the program to count the total number of words in the book,
# and the number of times each word is used.
# Print the number of different words used in the book. Compare different books by different authors,
# written in different eras. Which author uses the most extensive vocabulary?
#  -*- coding: utf-8 -*-


import string
import os

punctuations = [mark for mark in string.punctuation]
whitespaces = [char for char in string.whitespace]

origin = 'multilinguisme.txt'
depart = 'Dorian.txt'


def words(book):
    flag = False
    signal = "*** START OF"
    wo = []
    for w in book:
        if flag == True:
            for word in w.split():
                word = word.strip(string.punctuation + string.whitespace)
                word = word.lower()
                wo.append(word)
        elif (signal in w) and (flag == False):
            flag = True
        else:
            pass
    return wo


def histogram(data):
    hist = {}
    for word in data:
        hist[word] = hist.get(word, 0) + 1
    return hist


def nb(data):
    hist = {}
    for word in data:
        if word not in hist:
            hist[word] = 1
        else:
            hist[word] += 1
    return hist

books = [origin, depart]


def main():
    for book in books:
        book = open(book, 'r')
        print("Stats for %s:" % book.name)
        data = words(book)
        book.close()
        print("  Total: %s \n" % len(data))
        print("  Unique word: %s \n" % len(histogram(data)))
        print("  number of times each word is used -> %s \n" % nb(data))

def comp():
    # for book in books:
    book1 = open('multilinguisme.txt', 'r')
    book2 = open('Dorian.txt', 'r')
    data = words(book1)
    data1 = words(book2)
    if len(data) > len(data1):
        print('The Book multilinguisme.txt contain most extensive vocabularyt \n')
    else:
        print('The Book Doriand.txt contain most extensive vocabulary \n')
    book1.close()
    book2.close()

comp()
main()


os.system("pause")