__author__ = 'jb-flo'

"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 4
"""

import string
import os

punctuations = [mark for mark in string.punctuation]
whitespaces = [space for space in string.whitespace]
word_list = 'words.txt'


# def words():
#     flag = False
#     signal = "*** START OF"
#     data = open('multilinguisme.txt', 'r')
#     for line in data:
#         if flag == True:
#             for word in line.split():
#                 yield word
#         elif (signal in line) and (flag == False):
#             flag = True
#         else:
#             pass
#     data.close()


def words():
    flag = False
    signal = "*** START OF"
    data = open('multilinguisme.txt', 'r')
    wo = []
    for w in data:
        if flag == True:
            for word in w.split():
                word = word.strip(string.punctuation + string.whitespace)
                word = word.lower()
                wo.append(word)
        elif (signal in w) and (flag == False):
            flag = True
        else:
            pass
    data.close()
    return wo


def stats():
    book_words = set(words())
    words_ = set([word for word in open(word_list, 'r', encoding="utf8")])
    print("Stats for multilinguisme.txt")
    print("There are {} non-listed words.".format(len(book_words - words_)))

stats()

print("\n\nThe words not in the word list for multilinguisme.txt:")
print(set(words())-set([word for word in open(word_list, 'r', encoding="utf8")]))

os.system("pause")